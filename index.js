const express = require("express");
const app = express();
const {user_game_biodata} = require('./models')
const {user_game_history} = require('./models')
const {user_game} = require('./models')

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.post("/user", (req, res) => {
    const {name, password, email, birthdate, sex, address, city, country, phone_num, result, score} = req.body;

    user_game.create({
        name,
        password,
        email,
    }).then((user) => {
        user_game_history.create({
            score,
            result,
            UserId: user.id,
        }).then((user) => {
            console.log({ message: "Create Success", data: user});
        })

        user_game_biodata.create({
            birthdate, 
            sex, 
            address, 
            city, 
            country, 
            phone_num,
            UserId: user.id,
        }).then((user) => {
            console.log({ message: "Create Success", data: user});
        })

         res.json({ message: "Create Success", data: user});
    })
});

app.get("/users", (req, res) => {
    user_game.findAll({ include: ["userBiodata", "history"]}).then((users) => {
        res.json({message: "Fetch all success", data: users});
    });
});

app.get("/user/:id", (req, res) => {
    const {id} = req.params;

    user_game.findOne( { include: ["userBiodata", "history"]}, {where: { id: id }}).then((user) => {
        res.json({message: "User finded", data: user});
    })
});

app.delete("/user/:id", (req, res) => {
    const {id} = req.params;

    user_game.destroy({where: { id: id }}).then(() => {
        res.json({message: "Data deleted"});
    });
});

app.put("/user/:id", (req, res) => {
    const {id} = req.params;
    const {name, password, email} = req.body;

    user_game.update({
        name: name,
        password: password,
        email: email,
    }, {where: {id: id}})
    .then(() => {
        res.json({message: "Update Success"});
    })
    .catch((err)=> {
        res.json({message: "Update failed"});
    })
})

app.put("/biodata/:id", (req, res) => {
    const {id} = req.params;
    const {birthdate, sex, address, city, country, phone_num} = req.body;

    user_game_biodata.update({
        birthdate: birthdate, 
        sex: sex, 
        address: address, 
        city: city, 
        country: country, 
        phone_num: phone_num
    }, {where: {UserId: id}})
    .then(() => {
        res.json({message: "Update Success"});
    })
    .catch((err)=> {
        res.json({message: "Update failed"});
    })
})

app.put("/history/:userid/:id", (req, res) => {
    const {userid, id} = req.params;
    const {result, score} = req.body;

    user_game_history.update({
        result: result,
        score: score
    }, {where: {UserId: userid, id: id}})
    .then(() => {
        res.json({message: "Update Success"});
    })
    .catch((err)=> {
        res.json({message: "Update failed"});
    })
})

app.post("/history/:id", (req, res) => {
    const {id} = req.params;
    const {result, score} = req.body;

    user_game_history.create({
        result,
        score,
        UserId: id
    }, {where: {UserId: id}})
    .then(() => {
        res.json({message: "History Added"});
    })
    .catch((err)=> {
       res.json({message: "failed add history"});
    })
})

app.delete("/history/:userid/:id", (req, res) => {
    const {userid, id} = req.params;

    user_game_history.destroy({where: {UserId: userid, id: id}})
    .then(() => {
        res.json({message: "Delete Success"});
    })
    .catch((err)=> {
        res.json({message: "Delete failed"});
    })
})

app.listen(8000, () => console.log(`listening at http://localhost:${8000}`));